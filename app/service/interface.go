package service

import (
	"context"
	"net/url"

	"gitlab.com/lasiar/3rd/app/types"
)

type Tasker interface {
	// RunTask return id then run task async
	RunTask(ctx context.Context, method string, url url.URL, header map[string][]string) (int64, error)
	GetTask(ctx context.Context, id int64) (types.Task, error)
}
