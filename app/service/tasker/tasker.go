package tasker

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/lasiar/3rd/app/repo"
	"gitlab.com/lasiar/3rd/pkg/errors"
)

//go:generate minimock -i gitlab.com/lasiar/3rd/app/repo.Repo -o ./mocks/repo_mock.go
func New(r repo.Repo, processTimeout time.Duration) *Service {
	if processTimeout == 0 {
		processTimeout = time.Minute
	}
	return &Service{
		repo:    r,
		timeout: processTimeout,
		client:  http.DefaultClient,
	}
}

type Service struct {
	timeout time.Duration
	repo    repo.Repo
	client  *http.Client
}

func (*Service) validURL(u url.URL) error {
	switch "" {
	case u.Host:
		return errors.New("host must be not empty", errors.BadRequest)

	case u.Scheme:
		return errors.New("scheme must be not empty", errors.BadRequest)
	}

	if u.Scheme == "http" || u.Scheme == "https" {
		return nil
	}

	return errors.New(
		fmt.Sprintf("sheme must be %q or %q, got %q", "http", "https", u.Scheme),
		errors.BadRequest,
	)
}
