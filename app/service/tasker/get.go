package tasker

import (
	"context"

	"gitlab.com/lasiar/3rd/app/types"
)

func (s *Service) GetTask(ctx context.Context, id int64) (types.Task, error) {
	return s.repo.GetTask(ctx, id)
}
