package tasker

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"runtime/debug"
	"sync"
	"testing"
	"time"

	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
	"go.uber.org/zap/zaptest"

	"gitlab.com/lasiar/3rd/app/service/tasker/mocks"
	"gitlab.com/lasiar/3rd/app/types"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func TestService_RunTask(t *testing.T) {
	const createdID int64 = 1
	cleanHeaders := func(m map[string][]string) {
		for _, key := range []string{
			"Content-Length",
			"Date",
			"Content-Type",
			"Accept-Encoding",
			"User-Agent",
		} {
			delete(m, key)
		}

	}

	for _, tt := range []struct {
		name        string
		method      string
		reqHeaders  map[string][]string
		respHeaders map[string][]string
		respCode    int
		length      int64
	}{
		{
			name:     "simple",
			method:   http.MethodGet,
			respCode: http.StatusBadRequest,
			reqHeaders: map[string][]string{
				"Test-Req": {"1", "2"},
			},
			respHeaders: map[string][]string{
				"Test-Resp": {"2", "3"},
			},
			length: 4143,
		},
	} {
		t.Run(tt.name, func(t *testing.T) {
			// Check goroutine leak
			defer goleak.VerifyNone(t)

			// In test function running goroutine.
			wg := &sync.WaitGroup{}
			wg.Add(1)

			ctx := logger.Set(context.Background(), zaptest.NewLogger(t).Sugar())
			mockCtrl := minimock.NewController(t)
			defer func() {
				if r := recover(); r != nil {
					t.Errorf("panic %v\n%s", r, debug.Stack())
				}
				mockCtrl.Finish()
			}()

			server := httptest.NewServer(http.HandlerFunc(func(r http.ResponseWriter, w *http.Request) {
				require.Equal(t, tt.method, w.Method)
				cleanHeaders(w.Header)
				require.EqualValues(t, tt.reqHeaders, map[string][]string(w.Header))

				for key, values := range tt.respHeaders {
					for _, value := range values {
						r.Header().Add(key, value)
					}
				}

				r.WriteHeader(tt.respCode)
				_, err := io.Copy(r, bytes.NewReader(make([]byte, tt.length)))
				require.NoError(t, err)
			}))

			defer server.Close()
			u, err := url.Parse(server.URL)
			require.NoError(t, err)

			mock := mocks.NewRepoMock(mockCtrl)
			s := Service{
				// For easy debugging
				timeout: 10 * time.Minute,
				repo:    mock,
				client:  server.Client(),
			}

			mock.CreateTaskMock.Set(func(_ context.Context, task types.Task) (int64, error) {
				require.Equal(t, *u, task.URL)
				require.Equal(t, tt.method, task.Method)
				return createdID, nil
			})

			mock.SetStatusTaskMock.Set(func(_ context.Context, id int64, status types.Status) error {
				require.Equal(t, createdID, id)
				require.Equal(t, types.StatusInProcess, status)
				return nil
			})
			mock.DoneTaskMock.Set(func(_ context.Context, id int64, statusCode int, headers map[string][]string, length int64) (err error) {
				defer wg.Done()
				require.Equal(t, createdID, id)
				require.Equal(t, tt.respCode, statusCode)
				require.Equal(t, tt.length, length)

				cleanHeaders(headers)
				require.EqualValues(t, tt.respHeaders, headers)
				return nil
			})

			got, err := s.RunTask(ctx, tt.method, *u, tt.reqHeaders)
			require.NoError(t, err)
			require.Equal(t, createdID, got)
			wg.Wait()
		})
	}
}
