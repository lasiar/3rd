package tasker

import (
	"context"
	"io"
	"net/http"
	"net/url"

	"gitlab.com/lasiar/3rd/app/types"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func (s *Service) RunTask(ctx context.Context, method string, u url.URL, headers map[string][]string) (int64, error) {
	err := s.validURL(u)
	if err != nil {
		return 0, err
	}

	id, err := s.repo.CreateTask(ctx, types.Task{
		Method: method,
		URL:    u,
	})
	if err != nil {
		return 0, err
	}

	s.runTask(ctx, id, method, u, headers)

	return id, err
}

func (s *Service) runTask(
	ctx context.Context,
	id int64,
	method string,
	u url.URL,
	headers map[string][]string,
) {
	// Copy logger to new context, without cancel
	ctx = logger.Set(context.Background(), logger.Get(ctx))
	ctx, cancel := context.WithTimeout(ctx, s.timeout)

	go func() (err error) {
		defer func() {
			r := recover()
			switch {
			case r != nil:
				logger.Get(ctx).Errorf("got panic: %v", r)
			case err != nil:
				logger.Get(ctx).Errorf("%v", err)
			default:
				return
			}

			err = s.repo.SetStatusTask(context.Background(), id, types.StatusError)
			if err != nil {
				logger.Get(ctx).Errorf("set status: %#v", err)
			}

			cancel()
		}()

		err = s.repo.SetStatusTask(ctx, id, types.StatusInProcess)
		if err != nil {
			return err
		}

		lenBody, statusCode, headers, err := s.runRequest(ctx, method, u, headers)
		if err != nil {
			return err
		}

		return s.repo.DoneTask(ctx, id, statusCode, headers, lenBody)
	}() //nolint:errcheck
}

func (s *Service) runRequest(
	ctx context.Context,
	method string,
	u url.URL,
	headers map[string][]string,
) (int64, int, map[string][]string, error) {
	req, err := http.NewRequestWithContext(ctx, method, u.String(), http.NoBody)
	if err != nil {
		return 0, 0, nil, err
	}

	req.Header = headers

	resp, err := s.client.Do(req)
	if err != nil {
		return 0, 0, nil, err
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			logger.Get(ctx).Warnf("close body: %v", err)
		}
	}()

	// Yes, we can check len by header Content-Length, but:
	// 1. Content-Type is optional.
	// 2. HTTP client's Transport may not reuse HTTP/1.x if the Body is not read to completion (go doc).
	lenBody, err := io.Copy(io.Discard, resp.Body)
	if err != nil {
		return 0, 0, nil, err
	}

	return lenBody, resp.StatusCode, resp.Header, err
}
