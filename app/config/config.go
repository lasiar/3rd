package config

import (
	"time"

	"github.com/spf13/viper"

	"gitlab.com/lasiar/3rd/pkg/database"
)

type HTTP struct {
	Port              int
	ReadHeaderTimeout time.Duration
}

type ThirdService struct {
	TaskTimeout time.Duration
}

type Config struct {
	DB           database.Config
	ThirdService ThirdService
	HTTP         HTTP
}

func GetConfig(configPath string) (Config, error) {
	v := viper.New()
	v.SetConfigFile(configPath)

	err := v.ReadInConfig()
	if err != nil {
		return Config{}, err
	}

	// Setup default value.
	// viper.SetDefault is ugly.
	config := Config{
		ThirdService: ThirdService{TaskTimeout: time.Minute},
		HTTP: HTTP{
			ReadHeaderTimeout: 10 * time.Second,
		},
	}

	err = v.Unmarshal(&config)
	return config, err
}
