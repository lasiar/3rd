package rest

import (
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/lasiar/3rd/app/delivery/rest/generated/ops"
	"gitlab.com/lasiar/3rd/app/delivery/rest/serialize"
	"gitlab.com/lasiar/3rd/pkg/httperr"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func (h handler) getTaskID(p ops.GetTaskIDParams) middleware.Responder {
	ctx := p.HTTPRequest.Context()
	task, err := h.service.GetTask(ctx, p.ID)
	if err != nil {
		return httperr.NewResponder(err, logger.Get(ctx))
	}

	return ops.NewGetTaskIDOK().WithPayload(serialize.Task(task))
}
