package rest

import (
	"net/http"

	oaerr "github.com/go-openapi/errors"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime"
	oamiddleware "github.com/go-openapi/runtime/middleware"

	"gitlab.com/lasiar/3rd/app/delivery/rest/generated"
	"gitlab.com/lasiar/3rd/app/delivery/rest/generated/ops"
	"gitlab.com/lasiar/3rd/app/service"
	"gitlab.com/lasiar/3rd/pkg/middleware"
)

//go:generate swagger -q generate server -m generated/models -s generated -a ops -f ../../../api/swagger/swagger.yaml -C ../../../api/swagger/config.yaml
type handler struct {
	api     *ops.Nr3rdServiceAPI
	service interface {
		service.Tasker
	}
}

func New(queuer service.Tasker) (http.Handler, error) {
	swaggerSpec, err := loads.Embedded(generated.SwaggerJSON, generated.FlatSwaggerJSON)
	if err != nil {
		return nil, err
	}

	h := &handler{
		api:     ops.NewNr3rdServiceAPI(swaggerSpec),
		service: queuer,
	}

	h.api.ServeError = oaerr.ServeError
	h.api.JSONConsumer = runtime.JSONConsumer()
	h.api.JSONProducer = runtime.JSONProducer()

	h.api.PostTaskHandler = ops.PostTaskHandlerFunc(h.postTask)
	h.api.GetTaskIDHandler = ops.GetTaskIDHandlerFunc(h.getTaskID)

	return h.api.Serve(getMiddleware()), nil
}

func getMiddleware() oamiddleware.Builder {
	return func(next http.Handler) http.Handler {
		return middleware.RequestID(middleware.Logging(next))
	}
}
