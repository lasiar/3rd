package rest

import (
	"net/url"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/lasiar/3rd/app/delivery/rest/generated/ops"
	"gitlab.com/lasiar/3rd/app/delivery/rest/serialize"
	"gitlab.com/lasiar/3rd/pkg/errors"
	"gitlab.com/lasiar/3rd/pkg/httperr"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func (h handler) postTask(p ops.PostTaskParams) middleware.Responder {
	ctx := p.HTTPRequest.Context()

	u, err := url.Parse(p.Body.URL.String())
	if err != nil {
		return httperr.NewResponder(
			errors.Wrapf(err, errors.BadRequest, "invalid url %q", p.Body.URL.String()),
			logger.Get(ctx),
		)
	}

	id, err := h.service.RunTask(ctx, p.Body.Method, *u, p.Body.Headers)
	if err != nil {
		return httperr.NewResponder(err, logger.Get(ctx))
	}

	return ops.NewPostTaskOK().WithPayload(serialize.ID(id))
}
