package serialize

import "gitlab.com/lasiar/3rd/app/delivery/rest/generated/ops"

func ID(v int64) *ops.PostTaskOKBody {
	return &ops.PostTaskOKBody{ID: v}
}
