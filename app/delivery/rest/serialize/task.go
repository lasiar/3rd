package serialize

import (
	"github.com/go-openapi/swag"

	"gitlab.com/lasiar/3rd/app/delivery/rest/generated/models"
	"gitlab.com/lasiar/3rd/app/types"
)

func Task(task types.Task) *models.Task {
	return &models.Task{
		Headers:        task.Headers,
		HTTPStatusCode: ptrIntTo64(task.HTTPCode),
		Length:         task.Length,
		Status:         task.Status,
	}
}

func ptrIntTo64(v *int) *int64 {
	if v == nil {
		return nil
	}

	return swag.Int64(int64(*v))
}
