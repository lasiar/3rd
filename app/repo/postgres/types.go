package postgres

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"net/url"

	"gitlab.com/lasiar/3rd/app/types"
)

type pgHeaders map[string][]string

func (h *pgHeaders) Scan(src any) error {
	b, ok := src.([]byte)
	if !ok {
		return fmt.Errorf("want %T got %T", b, src)
	}

	return json.Unmarshal(b, &h)
}

func (h *pgHeaders) Value() (driver.Value, error) {
	return json.Marshal(h)
}

type pgTask struct {
	Status   string    `db:"status"`
	URL      string    `db:"url"`
	Method   string    `db:"method"`
	Headers  pgHeaders `db:"headers"`
	Length   *int64    `db:"length"`
	HttpCode *int      `db:"http_status_code"`
}

func (t pgTask) ToTask() (types.Task, error) {
	u, err := url.Parse(t.URL)
	if err != nil {
		return types.Task{}, err
	}

	return types.Task{
		Method:   t.Method,
		Headers:  t.Headers,
		URL:      *u,
		Length:   t.Length,
		Status:   t.Status,
		HTTPCode: t.HttpCode,
	}, nil
}
