package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgerrcode"

	"gitlab.com/lasiar/3rd/app/types"
	"gitlab.com/lasiar/3rd/pkg/errors"
)

func (r Repo) SetStatusTask(ctx context.Context, id int64, status types.Status) error {
	return r.updateTask(ctx, id, status, nil, nil, nil)
}

func (r Repo) DoneTask(ctx context.Context, id int64, statusCode int, headers map[string][]string, length int64) error {
	return r.updateTask(ctx, id, types.StatusDone, &statusCode, headers, &length)
}

func (r Repo) updateTask(
	ctx context.Context,
	id int64,
	status types.Status,
	statusCode *int,
	headers map[string][]string,
	length *int64,
) error {
	q := `/* update of task */
    update test.public.tasks
    set status  = :status,
        /* don't set null*/
        length  = coalesce(:length, length),
        headers = coalesce(:headers, headers),
    	http_status_code = coalesce(:status_code, http_status_code)
    where id = :id
`

	_, err := r.db.NamedExecContext(ctx, q, map[string]interface{}{
		"status":      status,
		"length":      length,
		"status_code": statusCode,
		"headers":     pgHeaders(headers),
		"id":          id,
	})
	return err
}

func (r Repo) CreateTask(ctx context.Context, task types.Task) (id int64, err error) {
	q := `/* save new task */
    insert into tasks (status, url, method)
    values ($1, $2, $3)
	returning id
`

	err = r.db.GetContext(ctx, &id, q, types.StatusNew, task.URL.String(), task.Method)
	if err == nil {
		return id, nil
	}

	var pgErr *pgconn.PgError
	if errors.As(err, &pgErr) &&
		pgerrcode.IsDataException(pgErr.Code) &&
		// Is it correct check by routine?
		pgErr.Routine == "enum_in" {
		return 0, errors.New(fmt.Sprintf("method %q is incorect", task.Method), errors.BadRequest)
	}

	return 0, err
}

func (r Repo) GetTask(ctx context.Context, id int64) (types.Task, error) {
	q := `/* get task */
    select status, url, method, headers, length, http_status_code
    from tasks
    where id = $1
`

	var tmp pgTask

	err := r.db.GetContext(ctx, &tmp, q, id)
	if err != nil {
		return types.Task{}, err
	}

	return tmp.ToTask()
}

func (r Repo) setError(ctx context.Context) error {
	q := `/* set all in_process -> error */
    update tasks
    set status = 'error'
    where status = 'in_process'
`
	_, err := r.db.ExecContext(ctx, q)
	return err
}
