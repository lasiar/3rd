package postgres

import (
	"context"

	"github.com/jmoiron/sqlx"
)

func New(ctx context.Context, db *sqlx.DB) (Repo, error) {
	r := Repo{db: db}
	// Because gracefull shutdown for service not implement, use this hack :(
	err := r.setError(ctx)
	return r, err
}

type Repo struct {
	db *sqlx.DB
}
