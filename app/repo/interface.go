package repo

import (
	"context"

	"gitlab.com/lasiar/3rd/app/types"
)

type Repo interface {
	GetTask(ctx context.Context, id int64) (types.Task, error)
	CreateTask(context.Context, types.Task) (int64, error)
	SetStatusTask(ctx context.Context, id int64, status types.Status) error
	DoneTask(ctx context.Context, id int64, statusCode int, headers map[string][]string, length int64) error
}
