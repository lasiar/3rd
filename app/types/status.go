package types

type Status string

const (
	StatusDone      Status = "done"
	StatusInProcess Status = "in_process"
	StatusError     Status = "error"
	StatusNew       Status = "new"
)
