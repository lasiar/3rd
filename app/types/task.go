package types

import "net/url"

type Task struct {
	Method   string
	Headers  map[string][]string
	URL      url.URL
	Length   *int64
	Status   string
	HTTPCode *int
}
