package main

import (
	"context"
	"net"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/lasiar/3rd/app/config"
	"gitlab.com/lasiar/3rd/app/delivery/rest"
	"gitlab.com/lasiar/3rd/app/repo/postgres"
	"gitlab.com/lasiar/3rd/app/service/tasker"
	"gitlab.com/lasiar/3rd/pkg/database"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func runApp(ctx context.Context, conf config.Config) {
	db, err := database.Get(ctx, conf.DB)
	if err != nil {
		logger.L().Fatal(err)
	}
	defer func() {
		err := db.Close()
		if err != nil {
			logger.L().Warnf("close db: %v", err)
		}
	}()

	repo, err := postgres.New(ctx, db)
	if err != nil {
		logger.L().Fatal(err)
	}

	handler, err := rest.New(tasker.New(repo, conf.ThirdService.TaskTimeout))
	if err != nil {
		logger.L().Fatal(err)
	}

	server := http.Server{
		Addr:              net.JoinHostPort("", strconv.Itoa(conf.HTTP.Port)),
		Handler:           handler,
		ReadHeaderTimeout: conf.HTTP.ReadHeaderTimeout,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			logger.L().Fatal(err)
		}
	}()

	<-ctx.Done()

	sdCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = server.Shutdown(sdCtx)
	if err != nil {
		logger.L().Warnf("shutdown server: %v", err)
	}
}
