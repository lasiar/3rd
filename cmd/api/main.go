package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/lasiar/3rd/app/config"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func initConfig() (config.Config, error) {
	path := "./config/local.yml"
	envPath := os.Getenv("CONFIG")
	if envPath != "" {
		path = envPath
	}

	return config.GetConfig(path)
}

func main() {
	_, isLogDev := syscall.Getenv("logger_dev")
	loggerEnv := logger.Production
	if isLogDev {
		loggerEnv = logger.Development
	}

	err := logger.InitGlobal(loggerEnv, "debug")
	if err != nil {
		logger.L().Fatal(err)
	}
	conf, err := initConfig()
	if err != nil {
		logger.L().Fatal(err)
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	runApp(ctx, conf)
}
