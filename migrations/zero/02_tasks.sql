create table if not exists tasks
(
    id               bigserial not null unique,
    status           status    not null,
    url              text      not null,
    method           method    not null,
    headers          jsonb,
    length           bigint,
    http_status_code int
);
