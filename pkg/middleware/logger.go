package middleware

import (
	"context"
	"net/http"
	"runtime/pprof"

	"go.uber.org/zap"

	"gitlab.com/lasiar/3rd/pkg/logger"
)

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			l := logger.Get(r.Context())

			ctx = logger.Set(ctx, l.With(
				zap.String("method", r.Method),
				zap.String("ip", r.RemoteAddr),
				zap.String("uri", r.RequestURI),
			))

			pprof.Do(
				ctx,
				pprof.Labels("method", r.Method, "ip", r.RemoteAddr, "uri", r.RequestURI),
				func(ctx context.Context) {
					next.ServeHTTP(w, r.WithContext(ctx))
				},
			)
		},
	)
}
