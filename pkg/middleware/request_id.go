package middleware

import (
	"context"
	"net/http"
	"runtime/pprof"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/lasiar/3rd/pkg/logger"
)

func RequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		reqID := uuid.New().String()
		l := logger.Get(r.Context())
		ctx = logger.Set(ctx, l.With(
			zap.String("request_id", reqID),
		))

		pprof.Do(
			ctx,
			pprof.Labels("request_id", reqID),
			func(ctx context.Context) {
				next.ServeHTTP(w, r.WithContext(ctx))
			},
		)
	},
	)
}
