package database

import (
	"context"
	"sync"

	"github.com/jackc/pgx/v4"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/lasiar/3rd/pkg/logger"
)

func newPgLogger() *pgLogger {
	return &pgLogger{
		fieldPool: sync.Pool{New: func() any {
			// Explanation why is pointer by link:
			// https://go-review.googlesource.com/c/go/+/24371/7..10/src/sync/example_pool_test.go#b12
			return &[]zapcore.Field{}
		}},
	}
}

type pgLogger struct {
	// fieldPool needs for zero allocations
	fieldPool sync.Pool
}

func (pl *pgLogger) Log(ctx context.Context, _ pgx.LogLevel, msg string, data map[string]interface{}) {
	z := logger.Get(ctx).Desugar()

	fields, _ := pl.fieldPool.Get().(*[]zapcore.Field)
	if cap(*fields) < len(data) {
		*fields = make([]zapcore.Field, 0, len(data))
	}

	*fields = (*fields)[:len(data)]
	i := 0
	for k, v := range data {
		(*fields)[i] = zap.Any(k, v)
		i++
	}

	z.Debug(msg, *fields...)
	pl.fieldPool.Put(fields)
}
