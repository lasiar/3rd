package database

type Migration struct {
	Zero string
}

type Config struct {
	Conn      string
	Migration Migration
	LogSQL    bool
}
