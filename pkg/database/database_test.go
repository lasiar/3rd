package database_test

import (
	"context"
	"os"
	"path/filepath"
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap/zaptest"

	"gitlab.com/lasiar/3rd/pkg/database"
	"gitlab.com/lasiar/3rd/pkg/logger"
)

func TestMigration(t *testing.T) {
	connString := os.Getenv("CONN_STRING")
	if connString == "" {
		t.Skipf("need CONN_STRING in env")
	}

	ctx := logger.Set(context.Background(), zaptest.NewLogger(t).Sugar())

	migrationDir, err := os.MkdirTemp("", "api_test")
	require.NoError(t, err)
	t.Cleanup(func() {
		err := os.RemoveAll(migrationDir)
		if err != nil {
			logger.Get(ctx).Warn(err)
		}
	})

	f, err := os.Create(filepath.Join(migrationDir, "test_migration.sql"))
	require.NoError(t, err)

	_, err = f.WriteString("create table test ()")
	require.NoError(t, err)

	err = f.Close()
	require.NoError(t, err)

	conf := database.Config{
		Conn:      connString,
		Migration: database.Migration{Zero: migrationDir},
		LogSQL:    true,
	}

	// permit migrations
	db, err := database.Get(ctx, conf)
	require.NoError(t, err)

	isApply, err := isTableExists(ctx, db, "test")
	require.NoError(t, err)
	require.True(t, isApply)

	err = db.Close()
	require.NoError(t, err)

	// ignore migrations
	_, err = database.Get(ctx, conf)
	require.NoError(t, err)
}

func isTableExists(ctx context.Context, db *sqlx.DB, tableName string) (b bool, err error) {
	q := `/* check if table exists */
	select exists(
               select
               from information_schema.tables
               where table_schema = 'public'
                 and table_name = $1
           )
`
	return b, db.GetContext(ctx, &b, q, tableName)
}
