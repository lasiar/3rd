package database

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"

	"gitlab.com/lasiar/3rd/pkg/logger"
)

const tableZeroMigration = "applied_zero_migration"

func Get(ctx context.Context, conf Config) (*sqlx.DB, error) {
	pgConf, err := pgx.ParseConfig(conf.Conn)
	if err != nil {
		return nil, err
	}

	pgConf.RuntimeParams["application_name"] = fmt.Sprintf("3rd_%s", os.Getenv("USER"))
	if conf.LogSQL {
		pgConf.Logger = newPgLogger()
	}

	db, err := sqlx.Connect("pgx", stdlib.RegisterConnConfig(pgConf))
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, applyZeroMigration(ctx, conf.Migration.Zero, db)
}

func applyZeroMigration(ctx context.Context, path string, db *sqlx.DB) (err error) {
	isApply, err := isApplyZeroMigration(ctx, db)
	if err != nil {
		return err
	}
	if isApply {
		return nil
	}

	dir, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	buf := &bytes.Buffer{}
	for _, entry := range dir {
		// skip all dirs, don't read sub dirs
		if entry.IsDir() {
			continue
		}

		err := readFile(buf, filepath.Join(path, entry.Name()))
		if err != nil {
			return err
		}
	}

	tx, err := db.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	_, err = tx.ExecContext(ctx, buf.String())
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, fmt.Sprintf("create table %s ()", tableZeroMigration))
	return err
}

func readFile(writer io.Writer, path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer func() {
		err := f.Close()
		if err != nil {
			logger.L().Warnf("close %q: %v", path, err)
		}
	}()

	_, err = io.Copy(writer, f)
	return err
}

func isApplyZeroMigration(ctx context.Context, db *sqlx.DB) (isApply bool, err error) {
	q := `/* Check if zero migration apply */
	select exists(
               select
               from information_schema.tables
               where table_schema = 'public'
                 and table_name = $1
           )
`
	return isApply, db.GetContext(ctx, &isApply, q, tableZeroMigration)
}
