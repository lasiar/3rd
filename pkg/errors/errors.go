package errors

import (
	"errors"
	"fmt"
)

type Error struct {
	// Reason what kind error
	Reason Reason
	// Message text for user
	Message string
	// Err wrapped error
	Err error
}

func (e *Error) Error() string {
	if e.Err == nil {
		return ""
	}

	if e.Message == "" {
		return e.Err.Error()
	}
	return e.Err.Error()
}

func (e *Error) Unwrap() error {
	return e.Err
}

func Wrapf(err error, reason Reason, format string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	return &Error{
		Reason:  reason,
		Message: fmt.Sprintf(format+": %v", append(args, err)...),
		Err:     err,
	}
}

func New(msg string, reason Reason) error {
	return &Error{
		Reason:  reason,
		Message: msg,
		Err:     errors.New(msg),
	}
}
