package errors

type Reason int

const (
	Internal Reason = iota
	BadRequest
	NotFound
)
