package httperr

import (
	stdErr "errors"
	"net/http"

	"github.com/go-openapi/runtime"
	"go.uber.org/zap"

	"gitlab.com/lasiar/3rd/pkg/errors"
)

type Responder struct {
	Message string `json:"message,omitempty"`
	Code    int    `json:"code,omitempty"`

	Logger *zap.SugaredLogger
}

func (r Responder) WriteResponse(w http.ResponseWriter, producer runtime.Producer) {
	w.WriteHeader(r.Code)
	w.Header().Set("Accept", "application/json")

	err := producer.Produce(w, r)
	if err != nil {
		r.Logger.Error(err)
	}
}

func NewResponder(err error, log *zap.SugaredLogger) Responder {
	resp := Responder{
		Message: "internal error",
		Code:    http.StatusInternalServerError,
		Logger:  log,
	}

	var t *errors.Error
	if !stdErr.As(err, &t) {
		return resp
	}

	resp.Message = t.Message
	switch t.Reason {
	case errors.Internal:
		resp.Code = http.StatusInternalServerError

	case errors.NotFound:
		resp.Code = http.StatusNotFound

	case errors.BadRequest:
		resp.Code = http.StatusBadRequest
	}

	return resp
}
