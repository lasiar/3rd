package logger

import (
	"context"

	"go.uber.org/zap"
)

type Environment int

const (
	Production Environment = iota
	Development
)

type loggerKey struct{}

var globalLogger *zap.SugaredLogger

func InitGlobal(env Environment, level string) (err error) {
	atomicLevel, err := zap.ParseAtomicLevel(level)
	if err != nil {
		return err
	}
	config := zap.Config{}

	switch env {
	case Production:
		config = zap.NewProductionConfig()

	case Development:
		config = zap.NewDevelopmentConfig()
	}

	config.Level = atomicLevel
	l, err := config.Build()
	if err != nil {
		return err
	}
	globalLogger = l.Sugar()
	return nil
}

func L() *zap.SugaredLogger {
	if globalLogger != nil {
		return globalLogger
	}

	return zap.L().Sugar()
}

func Get(ctx context.Context) *zap.SugaredLogger {
	l, _ := ctx.Value(loggerKey{}).(*zap.SugaredLogger)
	if l != nil {
		return l
	}

	return L()
}

func Set(ctx context.Context, logger *zap.SugaredLogger) context.Context {
	return context.WithValue(ctx, loggerKey{}, logger)
}
