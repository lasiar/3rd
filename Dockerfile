FROM golang:1.19-bullseye as builder

# get dependecies
COPY --from=quay.io/goswagger/swagger:v0.25.0 /usr/bin/swagger /usr/bin/swagger
RUN wget https://github.com/gojuno/minimock/releases/download/v3.0.9/minimock_3.0.9_linux_amd64.tar.gz && \
    tar xfv minimock_3.0.9_linux_amd64.tar.gz && \
    chmod +x minimock && \
    mv minimock /usr/bin/

COPY . /src
WORKDIR /src
RUN go generate ./... && go build -ldflags="-s -w" -o ./3rd ./cmd/api

FROM debian:stable-slim

RUN apt-get update && \
    apt-get install ca-certificates -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /app/config

COPY --from=builder /src/3rd /app/3rd
ENTRYPOINT ["/app/3rd"]
